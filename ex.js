var lwip = require('lwip');
var http = require('http');
var fs = require('fs');

var str = process.argv[2];
if (str === undefined) {
    console.log("usage : node ex $url of image$");
    process.exit(1);
}
var file = fs.createWriteStream("image.jpg");
var request = http.get(str, function(response) {
    response.on('end', alteredVersions).pipe(file);
}).on('error', function(err) {
    console.log(err.toString());
    process.exit(1);
});


function alteredVersions() {

    lwip.open("image.jpg", function(err, image) {
        errorHandler(err);
        image.resize(250, image.height(), function(err, image) {
            errorHandler(err);
            image.writeFile('output2.jpg', {
                'quality': 30
            }, function(err) {
                errorHandler(err);
                lwip.open("image.jpg", function(err, image) {
                    errorHandler(err);
                    image.resize(150, image.height(), function(err, image) {
                        image.writeFile('output1.jpg', {
                            'quality': 20
                        }, function(err) {
                            errorHandler(err);
                        });

                    });

                });
            });

        });

    })

}

function errorHandler(err) {

    if (err !== null) {
        console.log(err);
        process.exit(1);
    }
    return;
}   